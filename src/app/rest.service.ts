import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { EquipoPC } from './Modelo/equipo';
import { Observable } from 'rxjs';
import { post } from 'selenium-webdriver/http';
// import { HttpClientModule } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private cliente: HttpClient) { }

  getListadoEquipos(){
    return this.cliente.get('https://mnqy75unx6.execute-api.us-east-1.amazonaws.com/PruebasDemo/equipos');
  };

  // addEquipo(equipo:EquipoPC): Observable<any>{
  //   return this.cliente.post('https://qvy4rpa8vg.execute-api.us-east-1.amazonaws.com/pruebaAgregar/agregaequipo',equipo);
  // }

  postagregarEquipo(equipos){
    //return this.cliente.post('https://qvy4rpa8vg.execute-api.us-east-1.amazonaws.com/pruebaAgregar/agregaequipo',equipos);
    return this.cliente.post('https://qvy4rpa8vg.execute-api.us-east-1.amazonaws.com/pruebaAgregar/agregaequipo',equipos);
    //https://qvy4rpa8vg.execute-api.us-east-1.amazonaws.com/pruebaAgregar/agregaequipo
  }

}
