import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { EquiposComponent } from './coponents/equipos/equipos.component';
import { FiltrosComponent } from './components/filtros/filtros.component';
import { DescriEquipoComponent } from './descri-equipo/descri-equipo.component';
import { EquipoService } from './equipo.service';
import { AgregarequipoComponent } from './components/agregarequipo/agregarequipo.component';

@NgModule({
  declarations: [
    AppComponent,
    EquiposComponent,
    FiltrosComponent,
    DescriEquipoComponent,
    AgregarequipoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [EquipoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
