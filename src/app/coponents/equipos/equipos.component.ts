import { Component, OnInit, Inject } from '@angular/core';
import { RestService } from '../../rest.service';
import { TipoComputadora } from 'src/app/Modelo/TipoComputadora';
import { Router, ActivatedRoute } from '@angular/router';
import { EquipoPC } from '../../Modelo/equipo';
import { EquipoService } from '../../equipo.service'
import { Observable } from  'rxjs';


@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.css']
})

export class EquiposComponent implements OnInit {
  ListaEquipo: any;
  equipo2: EquipoPC[] = [];
  array = [];

  constructor(public listaequipos: RestService,
    private paginacion:Router,
    private route: ActivatedRoute,
    private equiposervice: EquipoService
    ) { 
  }

  ngOnInit(){
    this.listaequipos.getListadoEquipos()
    .subscribe(
      (equipos:any) => {
      this.ListaEquipo = equipos; // aqui trae toda el json completo
      //this.agregarequipoid =this.ListaEquipo.length + 1;
      this.equipo2 = equipos;
      // let data:any = equipos as []; 
      // let ordenado = data.map( // aqui solo selecciona los campos deseados
      //   g => {// mapping 
      //     //console.log("valor g", g)
      //     return {
      //       id: g.id, // using lodash to sum quantity
      //       TipoComputadora: g.TipoComputadora, // using lodash to sum price
      //     }
      //   })
      //   console.log("respuesta", ordenado)
  
        // var result = this.groupBy(ordenado, function(item)
        // {
        //   return [item.TipoComputadora];
        // });
        // console.log("respuesta group", result)
        // let acomodados = [];
        // for(let i = 0; i < result.length; i++){
        //   acomodados.push(result[i][0])
        // }
        // console.log("acomodados", acomodados)
    },
      err => console.log(err)
    )
  }

  // groupBy( array , f )
  // {
  //   var groups = {};
  //   array.forEach( function( o )
  //   {
  //     var group = JSON.stringify( f(o) );
  //     groups[group] = groups[group] || [];
  //     groups[group].push( o );  
  //   });
  //   return Object.keys(groups).map( function( group )
  //   {
  //     return groups[group]; 
  //   })
  // }


  DescripcionEquipo(e){
    this.paginacion.navigate(['equipo/descripcion/', e.id]);
    localStorage.setItem("Detalle", JSON.stringify((e)));
  }

  agregarequipo(){
    this.paginacion.navigate(['components/agregarequipo/']);
    localStorage.setItem("NuevoEquipo", "nuevo");
  }

}