import { Component, OnInit } from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {FlatTreeControl} from '@angular/cdk/tree';
import { RestService } from 'src/app/rest.service';
import { stringify } from '@angular/compiler/src/util';
import { TipoComputadora } from 'src/app/Modelo/TipoComputadora';
import { ConstantPool } from '@angular/compiler';


// interface FoodNode {
//   id: number;
//   children?: FoodNode[];
// }

// const TREE_DATA: FoodNode[] = [
//   {
//     name: 'Fruit',
//     children: [
//       {name: 'Apple'},
//       {name: 'Banana'},
//       {name: 'Fruit loops'},
//     ]
//   }, {
//     name: 'Vegetables',
//     children: [
//       {
//         name: 'Green',
//         children: [
//           {name: 'Broccoli'},
//           {name: 'Brussel sprouts'},
//         ]
//       }, {
//         name: 'Orange',
//         children: [
//           {name: 'Pumpkins'},
//           {name: 'Carrots'},
//         ]
//       },
//     ]
//   },
// ];

/** Flat node with expandable and level information */
// interface ExampleFlatNode {
//   expandable: boolean;
//   name: string;
//   level: number;
// }

@Component({
  selector: 'app-filtros',
  templateUrl: './filtros.component.html',
  styleUrls: ['./filtros.component.css']
})
export class FiltrosComponent implements OnInit {
  equipoPC: any;
  treetipocomputadoras: TipoComputadora[] = [];

  // private _transformer = (node: FoodNode, level: number) => {
  //   return {
  //     expandable: !!node.children && node.children.length > 0,
  //     name: node.name,
  //     level: level,
  //   };
  // }

//   treeControl = new FlatTreeControl<ExampleFlatNode>(
//     node => node.level, node => node.expandable);

// treeFlattener = new MatTreeFlattener(
//     this._transformer, node => node.level, node => node.expandable, node => node.children);

// dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(public listaequipos: RestService) { 
    // this.dataSource.data = TREE_DATA;
  }

  // hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  ngOnInit(){
    this.listaequipos.getListadoEquipos()
    .subscribe(
      equipos => {
      this.equipoPC = equipos;
      let arreglo:any = equipos as [];
      let tipComputadora = arreglo.map(
        mapeado => {
          return {
            id: mapeado.id,
            TipoComputadora: mapeado.TipoComputadora,
          }
        }
      )
      // El groupBy marcara error si no se tiene como funcion
      let resultado = this.groupBy(tipComputadora, function(item){
        return [item.TipoComputadora];
      });
      // console.log("respuesta del grupo filtros", resultado);
      let agrupados = [];
      for(let i = 0; i < resultado.length; i++){
        agrupados.push(resultado[i][0])
      }
      // console.log(agrupados);
      // aqui se le pasa al array 
      this.treetipocomputadoras = agrupados;
      // console.log(this.treetipocomputadoras);      
    },
      err => console.log(err)
    )
  }

//groupBy
groupBy(entradaArray, g){
  var grupos = {};
  entradaArray.forEach(function(a) {
    var grupo = JSON.stringify(g(a));
    grupos[grupo] = grupos[grupo] || [];
    grupos[grupo].push(a);
  });
  return Object.keys(grupos).map(function(grupo){
    return grupos[grupo];
  })

}


}
