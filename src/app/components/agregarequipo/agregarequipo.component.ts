import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/rest.service';
import { EquipoPC } from 'src/app/Modelo/equipo';
import { EquiposComponent } from 'src/app/coponents/equipos/equipos.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-agregarequipo',
  templateUrl: './agregarequipo.component.html',
  styleUrls: ['./agregarequipo.component.css']
})
export class AgregarequipoComponent implements OnInit {

  statusneworedit: any;
  agregarequipoid:any;
  equipo: object;
  equipo2: EquipoPC[] = [];
  nombredescripcioninput: string;
  precioinput:number;
  preciodescuentoinput:number;
  colorinput:string;
  procesadorinput:string;
  memoriaraminput: string;
  imageninput: string;
  caracteristicasBluetooinput: string;
  modeloprocesadorinpout: string;
  pesoinput: string;
  numpuertosusb3input: string;
  incluyeinput:string;
  otrainformacioninput:string;
  bluetoothinput:string;
  puertoethernetinput: string;
  cachememoriaraminput:string;
  puertohdmiInput:string;
  camarawebinput:string;
  puertosinput:string;
  camawebfrontalinput:string;
  compatibilidadinput: string;
  capamemoriagraficainput:string;
  resolucioninput:string;
  capacidaddiscoduroinput:string;
  sistemaopinput:string;
  softwareincluinput:string;
  tipocomputadorainput:string;
  sonidoinput:string;
  dimensionesinput:string;
  tamaniopantainput:string;
  dimensionesempaqueinput:string;
  tarjetagraficainput: string;
  duracionbateriainput:string;
  tipopantallainput:string;
  entradatarmemoriainput:string;
  productoinput:string;
  marcainput:string;
  submarcainput:string;
  tiposonidoinput:string;
  marcaprocesadorinput:string;
  tipotecladoinput:string;
  trackpadinput:string;
  velocidadprocesadorinput:string;

  constructor(public cliente: RestService,
    private paginacion: Router) { }
    
  ngOnInit() {
    this.statusneworedit = localStorage.getItem("NuevoEquipo")
    if (this.statusneworedit == "nuevo"){
      console.log(this.statusneworedit);
    }
    else{
      let detalleeditar = localStorage.getItem("detalleeditar");
      let detalleeditarjson = JSON.parse(detalleeditar)
      this.nombredescripcioninput = detalleeditarjson.NombreDescripcion;
      this.precioinput = detalleeditarjson.Precio;
  this.preciodescuentoinput = detalleeditarjson.Preciorebaja;
  this.colorinput = detalleeditarjson.Color;
  this.procesadorinput = detalleeditarjson.Procesador;
  this.memoriaraminput = detalleeditarjson.MemoriaRam;
  this.imageninput = detalleeditarjson.ImagenPrincipal;
  // this.caracteristicasBluetooinput = detalleeditarjson.Bluetooth;
  this.modeloprocesadorinpout = detalleeditarjson.ModeloProcesador;
  this.pesoinput = detalleeditarjson.Peso;
  this.numpuertosusb3input = detalleeditarjson.Puertos3;
  this.incluyeinput = detalleeditarjson.Incluye;
  // this.otrainformacioninput = detalleeditarjson.Preciorebaja;
  this.bluetoothinput = detalleeditarjson.Bluetooth;
  // this.puertoethernetinput = detalleeditarjson.ImagenPrincipal;
  // this.cachememoriaraminput = detalleeditarjson.Preciorebaja;
  this.puertohdmiInput = detalleeditarjson.Puerto_HDMI;
  this.camarawebinput = detalleeditarjson.CamaraWeb;
  this.puertosinput = detalleeditarjson.Puertos;
  // this.camawebfrontalinput = detalleeditarjson.Preciorebaja;
  // this.compatibilidadinput = detalleeditarjson.ImagenPrincipal;
  this.capamemoriagraficainput = detalleeditarjson.CapcMemoriaGrafica;
  this.resolucioninput = detalleeditarjson.Resolucion;
  this.capacidaddiscoduroinput = detalleeditarjson.Capacidad_Disco_Duro;
  this.sistemaopinput = detalleeditarjson.SistemaOperativo;
  // this.softwareincluinput = detalleeditarjson.Preciorebaja;
  this.tipocomputadorainput = detalleeditarjson.TipoComputadora;
  this.sonidoinput = detalleeditarjson.Sonido;
  this.dimensionesinput = detalleeditarjson.Dimensiones;
  this.tamaniopantainput = detalleeditarjson.Tamano_pantalla;
  this.dimensionesempaqueinput = detalleeditarjson.Dimensiones_Empaque;
  this.tarjetagraficainput = detalleeditarjson.TarjetaGraficos;
  this.duracionbateriainput = detalleeditarjson.DuracionBateria;
  // this.tipopantallainput = detalleeditarjson.Preciorebaja;
  this.entradatarmemoriainput = detalleeditarjson.Preciorebaja;
  this.productoinput = detalleeditarjson.Producto;
  this.marcainput = detalleeditarjson.Marca;
  this.submarcainput = detalleeditarjson.Submarca;
  this.tiposonidoinput = detalleeditarjson.TipoSonido;
  // this.marcaprocesadorinput = detalleeditarjson.Preciorebaja;
  // this.tipotecladoinput = detalleeditarjson.Preciorebaja;
  this.trackpadinput = detalleeditarjson.TrackPad;
  this.velocidadprocesadorinput = detalleeditarjson.VelocidadProcesador;
    }
  }

  agregarequiponuevo(){
    var equiponuevo: any;

    this.cliente.getListadoEquipos()
    .subscribe(
      (equipos:any) => {
      this.equipo2 = equipos;
      this.agregarequipoid = "";
      this.agregarequipoid = this.equipo2.length + 1;

      this.equipo ={
        id: this.agregarequipoid,
        NombreDescripcion: this.nombredescripcioninput,
        Peso: this.procesadorinput,
        Dimensiones_Empaque: this.dimensionesempaqueinput,
        DuracionBateria: this.duracionbateriainput,
        TarjetaGraficos: this.tarjetagraficainput,
        Tamano_pantalla: this.tamaniopantainput,
        Producto: this.productoinput,
        TipoSonido: this.tiposonidoinput,
        CapcMemoriaGrafica: this.capamemoriagraficainput,
        ImagenPrincipal: this.imageninput,
        Incluye: this.incluyeinput,
        Preciorebaja: this.preciodescuentoinput,
        Puertos: this.puertosinput,
        Sonido: this.sonidoinput,
        Puertos2: this.numpuertosusb3input,
        Puertos3: this.numpuertosusb3input,
        TamañoPantalla: this.tamaniopantainput,
        ModeloProcesador: this.modeloprocesadorinpout,
        Dimensiones: this.dimensionesinput,
        MemoriaRam: this.memoriaraminput,
        Marca: this.marcainput,
        Descripcion: this.nombredescripcioninput,
        SistemaOperativo: this.sistemaopinput,
        Resolucion: this.resolucioninput,
        Capacidad_Disco_Duro: this.capacidaddiscoduroinput,
        Puerto_HDMI: this.puertohdmiInput,
        TrackPad: this.trackpadinput,
        Procesador: this.procesadorinput,
        TipoComputadora: this.tipocomputadorainput,
        CamaraWeb: this.camarawebinput,
        VelocidadProcesador: this.velocidadprocesadorinput,
        Precio: this.precioinput,
        Submarca: this.submarcainput,
        Bluetooth: this.bluetoothinput,
        Color: this.colorinput,
        // CodigoProducto:'1078617051'
      } 

      this.cliente.postagregarEquipo(this.equipo)
      .subscribe(
        (equiponuevo:any) => {
          console.log("entra")
          equiponuevo = this.equipo;
          console.log("paso", equiponuevo)
          console.log("sale")
          this.paginacion.navigate(['']);
        },
        err => console.log(err)
      );
    },
      err => console.log(err)
    )
  }

}
