import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarequipoComponent } from './agregarequipo.component';

describe('AgregarequipoComponent', () => {
  let component: AgregarequipoComponent;
  let fixture: ComponentFixture<AgregarequipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarequipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarequipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
