import { FiltrosComponent } from './components/filtros/filtros.component';
import { AppComponent } from './app.component';
import { DescriEquipoComponent } from './descri-equipo/descri-equipo.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EquiposComponent } from './coponents/equipos/equipos.component';
import { AgregarequipoComponent } from './components/agregarequipo/agregarequipo.component';

const routes: Routes = [
  {path: '', component:EquiposComponent},
  {path:'', component:EquiposComponent},
  // {path:'', component:AgregarequipoComponent},
  {path: 'equipo/descripcion', component: DescriEquipoComponent},
  {path: 'equipo/descripcion/:id', component: DescriEquipoComponent},
  {path: 'components/agregarequipo', component:AgregarequipoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
