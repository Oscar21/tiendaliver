import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriEquipoComponent } from './descri-equipo.component';

describe('DescriEquipoComponent', () => {
  let component: DescriEquipoComponent;
  let fixture: ComponentFixture<DescriEquipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescriEquipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
