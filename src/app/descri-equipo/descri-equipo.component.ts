import { EquipoService } from './../equipo.service';
import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EquipoPC } from '../Modelo/equipo';

@Component({
  selector: 'app-descri-equipo',
  templateUrl: './descri-equipo.component.html',
  styleUrls: ['./descri-equipo.component.css']
})
export class DescriEquipoComponent implements OnInit {
  panelOpenState = false;
  panelOpenDescription = true;
  Equipo: EquipoPC[] = [];
  id: any;
  DescripcionEquipo: any;

  constructor(private paginacion:Router,
    private route: ActivatedRoute,
    private Equiposeleccionado: EquipoService
    ) {
   }

  ngOnInit() {
    let detalle = localStorage.getItem("Detalle");
    let detalleeditar = localStorage.getItem("Detalle");
    localStorage.setItem("detalleeditar", detalleeditar)
    this.DescripcionEquipo = JSON.parse(detalle);
    localStorage.removeItem('Detalle');
  }

  editarequipo(){
    this.paginacion.navigate(['components/agregarequipo/']);
    localStorage.setItem("NuevoEquipo", "editar");
  }

}
