import { NgModule } from '@angular/core'
import {
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatTreeModule,
    MatTableModule,
    MatListModule,
    MatExpansionModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
} from '@angular/material';


import {MatGridListModule} from '@angular/material/grid-list';


@NgModule({
    imports:[
        MatButtonModule,
        MatCardModule,
        MatToolbarModule,
        MatGridListModule,
        MatTreeModule,
        MatTableModule,
        MatListModule,
        MatExpansionModule,
        MatRadioModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule
    ],
    exports: [
        MatButtonModule,
        MatCardModule,
        MatToolbarModule,
        MatGridListModule,
        MatTreeModule,
        MatTableModule,
        MatListModule,
        MatExpansionModule,
        MatRadioModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule
    ]
})

export class MaterialModule {}