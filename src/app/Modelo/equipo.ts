export interface EquipoPC{
    Peso: number;
    Tamaño_Pantalla: number;
    Dimensiones_Empaque:string;
    TarjetaGraficos: string;
    Tamano_pantalla: number;
    Producto: string;
    NombreDescripcion: string;
    TipoSonido: string;
    CapcMemoriaGrafica: number;
    ImagenPrincipal: string;
    Incluye: string;
    Duracion_Batería: string;
    PrecioRebaja: number;
    Puertos: string;
    Sonido: string;
    Puertos2: string;
    id: number;
    Pantalla: string;
    Modelo_Procesador: string;
    Ram: string;
    Puertos3: string;
    Velocidad_Procesador: string;
    Dimensiones: string;
    Marca: string;
    SistemaOperativo: string;
    Resolucion: string;
    Capacidad_Disco_Duro: string;
    Puerto_HDMI: string;
    Procesador: string;
    TipoComputadora: string;
    CamaraWeb: number;
    Precio: number;
    Bluetooth: number;
    Color: number;
    TrackPad: string;
}